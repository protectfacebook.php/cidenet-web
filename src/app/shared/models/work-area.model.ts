
export interface IWorkArea{
  id?: number;
  description?: String;
}


export class WorkArea implements IWorkArea{
  constructor(
    public id?: number,
    public description?: String
  ){}
}
