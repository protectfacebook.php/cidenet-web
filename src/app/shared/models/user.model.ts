import { IWorkArea } from './work-area.model';
export interface IUser {
  id?: number;
  name?: String;
  otherName?: String;
  surname?: String;
  secondSurname?: String;
  email?: String;
  employmentCountry?: String;
  idType?: String;
  identificationNumber?: String;
  state?: Boolean;
  registrationDate?: String;
  admissionDate?: String;
  lastUpdate?: String;
  workArea?: IWorkArea;
}

export class User implements IUser{
  constructor(
    public id?: number,
    public name?: String,
    public otherName?: String,
    public surname?: String,
    public secondSurname?: String,
    public email?: String,
    public employmentCountry?: String,
    public idType?: String,
    public identificationNumber?: String,
    public state?: Boolean,
    public registrationDate?: String,
    public admissionDate?: String,
    public lastUpdate?: String,
    public workArea?: IWorkArea
  ){}
}
