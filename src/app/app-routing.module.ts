import { NavBarRoute } from './layouts/navbar/navbar.route';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

const LAYOUTS_ROUTES = [NavBarRoute]

const routes: Routes = [
  {
    path: 'core',
    loadChildren: ()=>  import('./components/components.module')
    .then(m => m.ComponentsModule)
  },
  {
    path: '',
    redirectTo: 'core',
    pathMatch: 'full'
  },
  ...LAYOUTS_ROUTES
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
