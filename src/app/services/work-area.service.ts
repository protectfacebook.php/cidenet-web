import { Observable } from 'rxjs';
import { IWorkArea } from './../shared/models/work-area.model';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WorkAreaService {

  URL_SERVER = environment.END_POINT;
  constructor(private Http: HttpClient) { }

  /**
   * create area
   * @param area
   */
  createArea(area: IWorkArea): Observable<IWorkArea>{
    return this.Http.post<IWorkArea>(`${this.URL_SERVER}/api/work-area`, area)
  }

  /**
   * update area
   * @param area
   */
  updateArea(area: IWorkArea): Observable<IWorkArea>{
    return this.Http.put<IWorkArea>(`${this.URL_SERVER}/api/work-area`, area)
  }

  /**
   * get all area
   */
  getAllAreas(): Observable<IWorkArea[]>{
    return this.Http.get<IWorkArea[]>(`${this.URL_SERVER}/api/work-area`)
  }

  /**
   * search area by id
   * @param id
   */
  findById(id: number): Observable<IWorkArea>{
    return this.Http.get<IWorkArea>(`${this.URL_SERVER}/api/work-area/${id}`)
  }

  /**
   * delete area
   * @param id
   */
  deleteArea(id: number): Observable<void>{
    return this.Http.delete<void>(`${this.URL_SERVER}/api/work-area/${id}`)
  }

}
