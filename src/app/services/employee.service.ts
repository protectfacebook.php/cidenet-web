import { IUser } from './../shared/models/user.model';
import { createRequestOption } from './../shared/utils/CreateRequestOption';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {  Observable } from 'rxjs';
import * as moment from 'moment'
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  URL_SERVER = environment.END_POINT
  constructor(private http: HttpClient) { }

  /**
   * create employee
   * @param employee
   */
  createEmployee(employee: IUser): Observable<IUser>{
    return this.http.post<IUser>(`${this.URL_SERVER}/api/users`, employee)
  }

  /**
   * delete employee
   * @param id
   */
  deleteEmployee(id: number): Observable<void>{
    return this.http.delete<void>(`${this.URL_SERVER}/api/users/${id}`)
  }

  /**
   * search employee by id
   * @param id
   */
  findById(id: number): Observable<IUser>{
    return this.http.get<IUser>(`${this.URL_SERVER}/api/users/${id}`)
    .pipe(map((res) => this.convertDateFromServer(res)) )
  }

  /**
   * update employee
   * @param employee
   */
  updateEmployee(employee: IUser): Observable<IUser>{
    return this.http.put<IUser>(`${this.URL_SERVER}/api/users`, employee)
  }

  /**
   * get employee pagination
   * @param req
   */
  getPaginatedEmployes(req: any): Observable<any>{
    const params = createRequestOption(req)
    return this.http.get<any>(`${this.URL_SERVER}/api/users/paginated`, {params})
    .pipe(map((res) =>  {return this.convertArrayDateFromServer(res)}))
  }

  /**
   * get employee pagination by criteria
   * @param req
   */
  getPaginatedEmployesByCriteria(req: any): Observable<any>{
    const params = createRequestOption(req)
    return this.http.get<any>(`${this.URL_SERVER}/api/users/paginated-by-criteria`, {params})
    .pipe(map((res) => this.convertArrayDateFromServer(res)))
  }

  /**
   * get employee pagination by state
   * @param req
   */
  getPaginatedEmployesByState(req: any): Observable<any>{
    const params = createRequestOption(req)
    return this.http.get<any>(`${this.URL_SERVER}/api/users/paginated-by-state`, {params})
    .pipe(map((res) => this.convertArrayDateFromServer(res)))
  }
  /**
   * get employee pagination by country
   * @param req
   */
  getPaginatedEmployesByEmployementCountry(req: any): Observable<any>{
    const params = createRequestOption(req)
    return this.http.get<any>(`${this.URL_SERVER}/api/users/paginated-by-employment-country`, {params})
    .pipe(map((res) => this.convertArrayDateFromServer(res)))
  }
  /**
   * get employee pagination by document number
   * @param req
   */
  getPaginatedEmployesByDocumentType(req: any): Observable<any>{
    const params = createRequestOption(req)
    return this.http.get<any>(`${this.URL_SERVER}/api/users/paginated-by-document-type`, {params})
    .pipe(map((res) => this.convertArrayDateFromServer(res)))
  }

  /**
   * convert array date
   * @param res
   * @private
   */
  private convertArrayDateFromServer(res: any): any{
    let employeeList: IUser[] = []
      employeeList = res.content
      employeeList.forEach(employee => {
        let dateList: any  = employee.registrationDate
        let date  = `${dateList[0]}-${dateList[1]}-${dateList[2]} ${dateList[3]}:${dateList[4]}:${dateList[5]}`
        employee.registrationDate = date

      })
      return res;
  }

  /**
   * convert array date
   * @param res
   * @private
   */
  private convertDateFromServer(res: IUser): IUser | any{
        let dateList: any  = res.registrationDate
        let date  = `${dateList[0]}-${dateList[1]}-${dateList[2]} ${dateList[3]}:${dateList[4]}:${dateList[5]}`
        res.registrationDate = date
        if(res.lastUpdate !== null && res.lastUpdate !== undefined){
         let lastUpdateList: any = res.lastUpdate
         let lastUpdateDate =  `${lastUpdateList[0]}-${lastUpdateList[1]}-${lastUpdateList[2]} ${lastUpdateList[3]}:${lastUpdateList[4]}:${lastUpdateList[5]}`
         res.lastUpdate = lastUpdateDate;
        }
        if(res.admissionDate !== null && res.admissionDate !== undefined){
          let admissionDateList: any = res.admissionDate
          let admissionDate = `${admissionDateList[0]}-${admissionDateList[1]}-${admissionDateList[2]}`
          res.admissionDate = moment(admissionDate).format("YYYY-MM-DD")
        }
        return res;
  }
}
