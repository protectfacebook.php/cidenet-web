import { NavbarComponent } from './navbar.component';

import {Route} from "@angular/router"


export const NavBarRoute: Route = {
  path: '',
  outlet: 'navbar',
  component: NavbarComponent
}
