import { WorkAreaService } from './../../services/work-area.service';
import { IWorkArea, WorkArea } from './../../shared/models/work-area.model';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-work-area-creation',
  templateUrl: './work-area-creation.component.html',
  styleUrls: ['./work-area-creation.component.css'],
})
export class WorkAreaCreationComponent implements OnInit {
  /**
   * creation response
   */
  objectCreationResponse = {
    response: false,
    correctCreationResponse: false,
    responseText: ''
  }
  /**
   * update response
   */
  objectUpdateResponse = {
    response: false,
    correctUpdateResponse: false,
    responseText: ''
  }

  /**
   * work area list variable of IWorkArea array
   */
  workAreasList?: IWorkArea[] = []
  /**
   * creation form values init and required params
   */
  creationForm = this.fb.group({
    description: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20)
    ])]
  })
  /**
   * update form values init and required params
   */
  updateForm = this.fb.group({
    id: [null, Validators.required],
    description: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20)
    ])]
  })

  constructor(private areaService: WorkAreaService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    /**
     * call loadData
     */
    this.loadData()
  }

  /**
   * create area to update, the item to return is IWorkArea
   */
  createNewAreaForUpdate(): IWorkArea{
    return{
      ...new WorkArea(),
      id: this.updateForm.get('id')!.value,
      description: this.updateForm.get('description')!.value
    }
  }

  /**
   * save area
   */
  saveArea(): void{
    this.areaService.createArea({
      description: this.creationForm.get('description')!.value
    })
    .subscribe((res) => {
      this.objectCreationResponse = {
        response: true,
        correctCreationResponse: true,
        responseText: `The object {${res.id}} was created`
      }
      this.loadData()
      this.creationForm.reset()
      setTimeout(() => {
        this.objectCreationResponse = {
          response: false,
          correctCreationResponse: false,
          responseText: ''
        }
      }, 2000);
    }, () => {
      this.objectCreationResponse = {
        response: true,
        correctCreationResponse: false,
        responseText: `An error has ocurred`
      }
    })
  }

  /**
   * get all areas
   */
  loadData(): void{
    this.areaService.getAllAreas()
    .subscribe(res => {
      this.workAreasList = res;
    })
  }

  /**
   * get info to load update form
   * @param id
   */
  fillUpdateForm(id: number): void{
    this.creationForm.reset()
    this.areaService.findById(id)
    .subscribe((res)=> {
      this.updateForm.patchValue({
        id: res.id,
        description: res.description
      })
    })
  }

  /**
   * update area
   */
  updateArea(): void{

    this.areaService.updateArea(this.createNewAreaForUpdate())
    .subscribe((res ) =>{
      this.objectUpdateResponse = {
        response: true,
        correctUpdateResponse: true,
        responseText: `Object {${res.id}} was updated`
      }
      this.updateForm.reset()
      this.loadData()
      setTimeout(()=> {
        this.objectUpdateResponse = {
          response: false,
          correctUpdateResponse: false,
          responseText: ''
        }
      }, 2000)
    }, () => {
      this.objectUpdateResponse = {
        response: true,
        correctUpdateResponse: false,
        responseText: `An error has ocurred`
      }
    })
  }

  /**
   * delete area
   * @param id
   */
  deleteArea(id: number): void {
    this.areaService.deleteArea(id)
    .subscribe(()=> {
      this.loadData()
    })
  }

}
