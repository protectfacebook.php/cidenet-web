import { WorkAreaService } from './../../services/work-area.service';
import { IWorkArea } from './../../shared/models/work-area.model';
import { FormBuilder, Validators } from '@angular/forms';
import { EmployeeService } from './../../services/employee.service';
import { IUser, User } from './../../shared/models/user.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment'
@Component({
  selector: 'app-employee-update',
  templateUrl: './employee-update.component.html',
  styleUrls: ['./employee-update.component.css']
})
export class EmployeeUpdateComponent implements OnInit {
  /**
   * Update response
   */
  updateResponse = {
    error: false,
    updated: false,
    responseText: '',
    response: false
  }
  /**
   * Date today variable
   */
  dateToday: String = '';

  /**
   * Min date variable
   */
  minDate: String = '';
  /**
   * Employee id variable
   */
  employeeId:number
  /**
   * Employee variable
   */
  employee: IUser
  /**
   * Work area list variable
   */
  workAreaList: IWorkArea[] = []

  /**
   * it's the update form
   */
  updateForm = this.fb.group({
    id: [],
    name: [null, Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    otherName: ['', Validators.compose([
      Validators.maxLength(50),
      Validators.pattern('[A-Z]{1,51}'),
    ])],
    surname: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    secondSurname: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    email: [''],
    employmentCountry: ['', Validators.required],
    workArea: ['', Validators.required],
    idType: [null, Validators.required],
    identificationNumber: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Za-z0-9]{1,21}')
    ])],
    admissionDate: [''],
    registrationDate: [''],
    lastUpdate: ['']
  })
  constructor(private activedRoute: ActivatedRoute,
              private areaService: WorkAreaService,
              private employeeService: EmployeeService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.employeeId = JSON.parse(this.activedRoute.snapshot.paramMap.get('id'))
    if(this.employeeId !== null && this.employeeId !== undefined){
      this.setMinDateAndActuallyDate()
      this.loadData()
    }
  }

  /**
   * this method return a new IUser intface from the updateForm data
   */

  createNewUserObjectForUpdate(): IUser {
    return{
      ...new User(),
      id: this.updateForm.get('id')!.value,
      name: this.updateForm.get('name')!.value,
      otherName: this.updateForm.get('otherName')!.value,
      surname: this.updateForm.get('surname')!.value,
      secondSurname: this.updateForm.get('secondSurname')!.value,
      employmentCountry: this.updateForm.get('employmentCountry')!.value,
      workArea: this.updateForm.get('workArea')!.value,
      idType: this.updateForm.get('idType')!.value,
      identificationNumber: this.updateForm.get('identificationNumber')!.value,
      admissionDate: moment(this.updateForm.get('admissionDate')!.value).format('YYYY-MM-DD'),
      email: this.updateForm.get('email')!.value
    }
  }

  /**
   *
   * @param id is an optional param from load information
   * the information to load is an area list and the actually employee information
   */

  loadData(id?: number): void{
    this.areaService.getAllAreas()
    .subscribe(res => {
      this.workAreaList = res
    })
    this.employeeService.findById(id || this.employeeId)
    .subscribe(res => {
      this.employee = res
      this.patchFormValues(res)
    })

  }



  /**
   * this method set the minDate for the admissionDate
   */

  setMinDateAndActuallyDate(): void{
    let  date = new Date();
    let  month = this.pad2(date.getMonth()+1);
    let minMonth = this.pad2((date.getMonth() +1) - 1)
    let  day = this.pad2(date.getDate());
    let  year= date.getFullYear();
    this.dateToday = year+"-"+month+"-"+day;
    this.minDate = year+"-"+minMonth+"-"+day
  }
  /**
   *
   * @param n this parameter is a number
   * this method verify the @param n value, with this validation the method return a string with a character and n
   */
  pad2(n): String | number{
    return (n < 10 ? '0' : '') + n;
  }

  /**
   * This method is for employe update
   * after update this method change any response variables for defining  the interface activity
   */

  updateEmployee(): void{
    let employee: IUser = this.createNewUserObjectForUpdate()
    this.employeeService.updateEmployee(employee)
    .subscribe(res => {
      this.updateResponse.error = false
      this.updateResponse.response = true
      this.updateResponse.updated = true
      this .updateResponse.responseText = `Employe with id{${res.id}} was updated`
      this.loadData()
    }, error => {
      this.updateResponse.response = true
      this.updateResponse.error = true
      this.updateResponse.updated = false
      if(error.error.description){
        this.updateResponse.responseText = error.error.description
      }else{
        this.updateResponse.responseText = "An error was ocurred"
      }

    })

  }

  /**
   * load data to update form
   * @param employee
   */
  patchFormValues(employee?: IUser): void{
    this.updateForm.patchValue({
      id: employee.id,
      name: employee.name,
      otherName: employee.otherName,
      surname: employee.surname,
      secondSurname: employee.secondSurname,
      email: employee.email,
      employmentCountry: employee.employmentCountry,
      workArea: employee.workArea,
      idType: employee.idType,
      identificationNumber: employee.identificationNumber,
      admissionDate: employee.admissionDate,
      registrationDate: employee.registrationDate,
      lastUpdate : employee.lastUpdate
    })
  }

}
