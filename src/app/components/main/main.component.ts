import { FormBuilder, Validators } from '@angular/forms';
import { IUser } from './../../shared/models/user.model';
import { EmployeeService } from './../../services/employee.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  /**
   * Delete response
   */
  deleteResponse = {
    error : false,
    success : false
  }
  /**
   * Change page option variable
   */
  changePageOption = 0
  /**
   * display variable to show element
   */
  display = false;
  /**
   * Selected employee for delete IUser interface
   */
  selectedEmployeeForDelete?: IUser;
  /**
   * Employee list IUser interface array
   */
  employesList?: IUser[] = [];
  /**
   * Size page variable
   */
  size = 10
  /**
   * Page number variable
   */
  pageNumber = 0
  /**
   * Total element variable
   */
  totalElements = 0
  /**
   * Total pages element
   */
  totalPages = 0
  /**
   * Pager variable
   */
  pager: number[] = []

  /**
   * Search form init values and required params
   */
  searchForm= this.fb.group({
    state: [null,  Validators.required],
    employmentCountry: ['', Validators.required],
    documentType: ['', Validators.required]
  })
  /**
   * Criteria variable
   */
  criteria: String = ''
  constructor(private employeeService: EmployeeService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    /**
     * Call loadData
     */
    this.loadData()
  }

  /**
   * Gets employees with pagination params
   * @param page optional
   */
  loadData(page?: number): void{
    this.pageNumber = page !== undefined ? page : 0;
    this.employeeService.getPaginatedEmployes({
      page: this.pageNumber,
      size: this.size
    }).subscribe(res => {
      /**
       * Call setUsersAndPaginationInfo
       */
      this.setUsersAndPaginationInfo(res)
      this.changePageOption = 0
    }, error => {
      console.log(error)
    })
  }

  /**
   * Select user to delete
   * @param user
   */
  setSelectedUser(user: IUser): void{
    this.selectedEmployeeForDelete = user
  }

  /**
   * Restart select user
   */
  resetSelectedUser(): void{
    this.display = false
    this.selectedEmployeeForDelete = {}
  }

  /**
   * Delete employee with selectedEmployeeForDelete.id param
   */
  deleteEmployee(): void{
    this.employeeService.deleteEmployee(this.selectedEmployeeForDelete.id)
      .subscribe(()=> {
        this.loadData()
        this.display = false
        this.selectedEmployeeForDelete = {}
        this.deleteResponse.success = true
        this.deleteResponse.error = false
      }, ()=> {
        this.deleteResponse.success = false
        this.deleteResponse.error = true
      })
  }

  /**
   * search employee by criteria request
   * @param page
   */
  searchEmployeeByCriteria(page?: number): void{
    this.pageNumber = page != undefined ? page : 0;
    if(this.criteria.length > 1){
      this.employeeService.getPaginatedEmployesByCriteria({
        criteria: this.criteria,
        page: this.pageNumber,
        size: this.size
      })
      .subscribe(res => {
        this.setUsersAndPaginationInfo(res)
        this.changePageOption = 1
      })
    }
  }

  /**
   * Get total page
   * @param totalPages
   */
  fillPagerNumberPages(totalPages: number){
    this.pager = []
    for(let i = 0; i <totalPages; i++) this.pager.push(i)
  }

  /**
   * search employee by country
   * @param page
   */
  searchByEmployementCountry(page?: number): void{
    this.pageNumber = page != undefined ? page : 0;
    this.searchForm.patchValue({
      state: null,
      documentType: ''
    })
    this.employeeService.getPaginatedEmployesByEmployementCountry({
      country: this.searchForm.get('employmentCountry')!.value,
      page: this.pageNumber,
      size: this.size
    }).subscribe(res => {
      this.changePageOption = 2
      this.setUsersAndPaginationInfo(res)
    })
  }

  /**
   * search employee by document number
   * @param page
   */
  searchByDocumentType(page?: number): void{
    this.pageNumber = page != undefined ? page : 0;
    this.searchForm.patchValue({
      state: null,
      employmentCountry: ''
    })
    this.employeeService.getPaginatedEmployesByDocumentType({
      documentType: this.searchForm.get('documentType')!.value,
      page: this.pageNumber,
      size: this.size
    }).subscribe(res => {
      this.changePageOption = 3
      this.setUsersAndPaginationInfo(res)
    })
  }

  /**
   * search employee by state
   * @param page
   */
  searchByState(page?: number): void{
    this.pageNumber = page != undefined ? page : 0;
    this.searchForm.patchValue({
      documentType: '',
      employmentCountry: ''
    })
    this.employeeService.getPaginatedEmployesByState({
      state: this.searchForm.get('state')!.value,
      page: this.pageNumber,
      size: this.size
    }).subscribe(res => {
      this.setUsersAndPaginationInfo(res)
      this.changePageOption = 4
    })
  }


  /**
   * get users and pagination info
   * @param res
   */
  setUsersAndPaginationInfo(res: any): void{
      this.employesList = res.content
      this.totalElements = res.totalElements
      this.totalPages = res.totalPages
      this.fillPagerNumberPages(res.totalPages)
  }

  /**
   * clear filter to search employee
   */
  cleanFilters(): void{
    this.criteria = ''
    this.changePageOption = 0
    this.searchForm.reset()
    this.loadData()
  }

  /**
   * change page option to select which option prefer
   * @param pageNumber
   */
  changePage(pageNumber: number): void{
    switch (this.changePageOption) {
      case 1:
        this.searchEmployeeByCriteria(pageNumber)
        break
      case 2:
        this.searchByEmployementCountry(pageNumber)
        break
      case 3:
        this.searchByDocumentType(pageNumber)
        break
      case 4:
        this.searchByState(pageNumber)
        break
      case 0:
        this.loadData(pageNumber);
        break
      default:
        this.loadData(pageNumber)
        break
    }
  }
}
