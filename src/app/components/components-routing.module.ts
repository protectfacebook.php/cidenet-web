import { WorkAreaCreationComponent } from './work-area-creation/work-area-creation.component';
import { EmployeeUpdateComponent } from './employee-update/employee-update.component';
import { EmployeeCreationComponent } from './employee-creation/employee-creation.component';
import { MainComponent } from './main/main.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'main',
    component: MainComponent,
  },
  {
    path: 'create-employee',
    component: EmployeeCreationComponent
  },
  {
    path: 'update-employee/:id',
    component: EmployeeUpdateComponent
  },
  {
    path: 'work-areas',
    component: WorkAreaCreationComponent
  },
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ComponentsRoutingModule { }
