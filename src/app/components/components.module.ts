import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsRoutingModule } from './components-routing.module';
import { MainComponent } from './main/main.component';

import { EmployeeCreationComponent } from './employee-creation/employee-creation.component';
import { EmployeeUpdateComponent } from './employee-update/employee-update.component';
import { WorkAreaCreationComponent } from './work-area-creation/work-area-creation.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [MainComponent, EmployeeCreationComponent, EmployeeUpdateComponent, WorkAreaCreationComponent],
  imports: [
    CommonModule,
    ComponentsRoutingModule,
    ReactiveFormsModule,
    FormsModule

  ]
})
export class ComponentsModule { }
