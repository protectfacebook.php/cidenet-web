import { EmployeeService } from './../../services/employee.service';
import { IUser, User } from './../../shared/models/user.model';
import { FormBuilder, Validators } from '@angular/forms';
import { IWorkArea } from './../../shared/models/work-area.model';
import { WorkAreaService } from './../../services/work-area.service';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment'

@Component({
  selector: 'app-employee-creation',
  templateUrl: './employee-creation.component.html',
  styleUrls: ['./employee-creation.component.css']
})
export class EmployeeCreationComponent implements OnInit {

  /**
   * Creation response
   */
  creationResponse = {
    response: false,
    wasCreated: false,
    error: false,
    responseText: ''
  }
  /**
   * Date today variable
   */
  dateToday: String = '';
  /**
   * Date min variable
   */
  minDate: String = '';
  /**
   * Work area list variable interface IWorkArea array
   */
  workAreaList?: IWorkArea[] = []
  /**
   * Init values creation form and required params
   */
  creationForm = this.fb.group({
    name: [null, Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    otherName: ['', Validators.compose([
      Validators.maxLength(50),
      Validators.pattern('[A-Z]{1,51}'),
    ])],
    surname: [null, Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    secondSurname: [null, Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Z]{1,21}'),
    ])],
    employmentCountry: ['', Validators.required],
    workArea: [null, Validators.required],
    idType: [null, Validators.required],
    identificationNumber: ['', Validators.compose([
      Validators.required,
      Validators.maxLength(20),
      Validators.pattern('[A-Za-z0-9]{1,21}')
    ])],
    admissionDate: ['']

  })

  constructor(private areaService: WorkAreaService,
              private employeeService: EmployeeService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    /**
     * Call loadAreas
     */
    this.loadAreas()
    /**
     * Call setMinDateAndActuallyDate
     */
    this.setMinDateAndActuallyDate()
  }

  /**
   * Get all areas of areaService and saved work area list
   */
  loadAreas(): void{
    this.areaService.getAllAreas().subscribe(res => {
      this.workAreaList = res
    })
  }

  /**
   * Create form to save User
   */
  createNewUser(): IUser{
    return{
      ...new User(),
      name: this.creationForm.get('name')!.value,
      otherName: this.creationForm.get('otherName')!.value,
      surname: this.creationForm.get('surname')!.value,
      secondSurname: this.creationForm.get('secondSurname')!.value,
      employmentCountry: this.creationForm.get('employmentCountry')!.value,
      workArea: this.creationForm.get('workArea')!.value,
      idType: this.creationForm.get('idType')!.value,
      identificationNumber: this.creationForm.get('identificationNumber')!.value
    }
  }

  /**
   * Save and get date element to dateToday and minDate variables
   */
  setMinDateAndActuallyDate(): void{
    let  date = new Date();
    let  month = this.pad2(date.getMonth()+1);
    let minMonth = this.pad2((date.getMonth() +1) - 1)
    let  day = this.pad2(date.getDate());
    let  year= date.getFullYear();
    this.dateToday = year+"-"+month+"-"+day;
    this.minDate = year+"-"+minMonth+"-"+day
  }

  /**
   * this method verify the @param n value, with this validation the method return a string with a character and n
   * @param n
   */
  pad2(n): String | number{
    return (n < 10 ? '0' : '') + n;
  }

  /**
   * Save user
   */
  saveUser(): void{
    let employee = this.createNewUser()
    if(this.creationForm.get('admissionDate')!.value !== ''){
      employee.admissionDate = moment(this.creationForm.get('admissionDate')!.value).format('YYYY-MM-DD')
    }
    this.employeeService.createEmployee(employee)
    .subscribe(res => {
      this.creationResponse = {
        response: true,
        wasCreated: true,
        responseText:`User  id{${res.id}} was created with the email {${res.email}}`,
        error: false
      }
      this.creationForm.reset()
    }, error => {
      this.creationResponse.response = true
      this.creationResponse.wasCreated = false
      this.creationResponse.error = true
      if(error.error.description){
        this.creationResponse.responseText = error.error.description
      }else{
        this.creationResponse.responseText = 'An error was ocurred'
      }
    })
  }

}
